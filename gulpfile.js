var
    gulp = require('gulp')

/*++dev*/
    , clean = require('gulp-clean')
    , connect = require('gulp-connect')
    , watch = require('gulp-watch')
    , batch = require('gulp-batch')
    , sourcemaps = require('gulp-sourcemaps')
    , plumber = require('gulp-plumber')
/*dev--*/

/*++css*/
    , sass = require('gulp-sass')
    , postcss = require('gulp-postcss')
    , autoprefixer = require('autoprefixer')
    , cssnano = require('cssnano')
/*css--*/
    ;

var
    srcRoot = 'src'
    , destRoot = 'dest'
    , path = {
        src: {
            indexFile: srcRoot + '/index.html'
            , scss: srcRoot + '/scss/**/*.scss'
            , img: srcRoot + '/img/**/*'
            , font: srcRoot + '/fonts/**/*'
            , js: srcRoot + '/js/**/*.js'
        }
        , dest: {
            css: destRoot + '/css/'
            , img: destRoot + '/img'
            , font: destRoot + '/fonts'
            , js: destRoot + '/js'
        }
    };

var
    bowerComponents = 'bowerComponents'
    , vendor = {
        css: {
            normalizeCss: bowerComponents + '/normalize-css/normalize.css'
        },
        js: {
            jquery: bowerComponents + '/jquery/dist/jquery.min.js'
        }
    };

var
    tasks = {
        copyAll: 'copyAll'
        , copyIndexFile: 'copyIndexFile'
        , css: 'css'
        , copyImg: 'copyImg'
        , copyFont: 'copyFont'
        , copyJs: 'copyJs'
        , clean: 'clean'
        , connect: 'connect'
        , watch: 'watch'
        , livereload: 'livereload'
        , dev: 'dev'
    };

var host = {
    port: 8888
};

/**
 * Копирование всех исходников в директорию дистрибутива
 */
gulp.task(tasks.copyAll, [
    tasks.copyIndexFile, tasks.css, tasks.copyImg, tasks.copyFont, tasks.copyJs
]);

/**
 * Копирование файла index.html
 */
gulp.task(tasks.copyIndexFile, function () {
    return gulp.src(path.src.indexFile)
        .pipe(plumber())
        .pipe(gulp.dest(destRoot));
});

/**
 * Создание файла стилей
 */
gulp.task(tasks.css, function () {
    var processors = [
        autoprefixer({browsers: ['last 5 version']})
        // , cssnano()
    ];

    return gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(plumber())
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.dest.css));
});

/**
 * Копирование изображений
 */
gulp.task(tasks.copyImg, function () {
    return gulp.src(path.src.img)
        .pipe(plumber())
        .pipe(gulp.dest(path.dest.img));
});

/**
 * Копирование шрифтов
 */
gulp.task(tasks.copyFont, function () {
    return gulp.src(path.src.font)
        .pipe(plumber())
        .pipe(gulp.dest(path.dest.font));
});

/**
 * Копирование скриптов
 */
gulp.task(tasks.copyJs, function () {
    return gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(gulp.dest(path.dest.js));
});

/**
 * Очистка директории [destRoot]
 */
gulp.task(tasks.clean, function () {
    gulp.src(destRoot, {read: false})
        .pipe(plumber())
        .pipe(clean({force: true}));
});

/**
 * Запуск локального сервера на [host.port] порту
 */
gulp.task(tasks.connect, function () {
    connect.server({
        root: destRoot,
        livereload: true,
        port: host.port
    });
});

/**
 * Слежение за файлами исходников
 */
gulp.task(tasks.watch, function () {
    watch(path.src.indexFile, batch(function (events, done) {
        gulp.start(tasks.copyIndexFile, done);
    }));
    watch(path.src.scss, batch(function (evens, done) {
        gulp.start(tasks.css, done);
    }));
    watch(path.src.img, batch(function (events, done) {
        gulp.start(tasks.copyImg, done);
    }));
    watch(path.src.js, batch(function (events, done) {
        gulp.start(tasks.copyJs, done);
    }));
});

/**
 * Обновление страницы в браузере при изменениях в [destRoot]
 */
gulp.task(tasks.livereload, function () {
    watch(destRoot).pipe(connect.reload());
});

/**
 * Старт сервера, слежения за файлами исходников и обновления страницы в браузере
 */
gulp.task(tasks.dev, [tasks.copyAll, tasks.connect, tasks.watch, tasks.livereload]);





