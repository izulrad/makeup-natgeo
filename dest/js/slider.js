function Slider(sliderId) {
    this.id = sliderId;
    this.slider = document.getElementById(this.id);
    this.images = document.createElement("div");
    this.links = document.createElement("div");
    this.captions = document.createElement("div");
    this.pictures = [];
    this.current = 0;


    this.addPictures = function (pics) {
        this.pictures = this.pictures.concat(pics);
    };

    this.init = function () {
        var scope = this;

        scope.slider.appendChild(scope.links);
        scope.slider.appendChild(scope.images);
        scope.slider.appendChild(scope.captions);


        scope.slider.className = "slider";
        scope.captions.className = "caption";
        scope.links.className = "links";
        scope.images.className = "images";

        scope.pictures.forEach(function (pic, key) {
            /*img create*/
            var img = document.createElement("img");
            img.src = pic.src;
            scope.images.appendChild(img);

            /*link create*/
            var link = document.createElement("p");
            link.appendChild(document.createTextNode(key + 1));
            link.onclick = function () {
                scope.clear();
                scope.current = this.textContent - 1;
                scope.setCurrent();
            };
            scope.links.appendChild(link);

            /*caption create*/
            var article = scope.captions.appendChild(
                document.createElement("article")
            );
            var content = article.appendChild(
                document.createElement("div")
            );
            content.appendChild(
                document.createElement("h3")
            ).appendChild(
                document.createTextNode(pic.caption.title)
            );
            content.appendChild(
                document.createElement("p")
            ).appendChild(
                document.createTextNode(pic.caption.text)
            );

            var btn = document.createElement("button");
            btn.onclick = function () {
                alert("Dunno what should i do here :(");
            };
            article.appendChild(btn);
        });

        scope.setCurrent();
    };

    this.clear = function () {
        this.images.childNodes[this.current].className = "";
        this.links.childNodes[this.current].className = "";
        this.captions.childNodes[this.current].className = "";
    };

    this.setCurrent = function () {
        this.images.childNodes[this.current].className = "current";
        this.links.childNodes[this.current].className = "current";
        this.captions.childNodes[this.current].className = "current";
    };

}
